<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="../css/inputstyle.css">
    <link rel="stylesheet" href="../css/style.css">
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
        crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" crossorigin="anonymous"></script>
    <script type="text/javascript" src="../js/jquery-3.4.1.min.js"></script>
    <script src="../js/task.js"></script>
    <script src="../js/function.js"></script>
    <!-- <script type="text/javascript" src="./libs/flashcanvas.js"></script> -->
    <script type="text/javascript" src="../libs/jSignature.min.js"></script>
    <title>DONK CLUB</title>
</head>
<?php
    $host         = "localhost";
    $username     = "root";
    $password     = "";
    $dbname       = "singnature";

    /* Create connection */
    $conn = new mysqli($host, $username, $password, $dbname);
    /* Check connection */
    if ($conn->connect_error) {
        die("Connection to database failed: " . $conn->connect_error);
    }
    $user_id = $_GET['id'];
    $sql = "SELECT * FROM user WHERE id = $user_id";
    $result = $conn->query($sql);
    $row = mysqli_fetch_array($result);
?>
<body>
    <div class="container" id="content">
        <div class="empty"></div>
        <div class="border title center">
            <h1>DONK CLUB</h1>
            <h2>CIRCOLO AFFICIATO FENALC</h2>
        </div>
        <br>
        <div class="center">
            <h3 id="bg_grey">DOMANDA DI AMMISSIONE A SOCIO</h3>
            <p class="rofa">(Ai sensi dello Statuto Sociale)</p>
        </div>
        <div>
            <table class="center mytable">
                <tbody>
                    <tr>
                        <td>
                            <span>
                                <input id="RINNOVO" type="radio" name="RINNOVO" value="RINNOVO" <?php  if ($row[1] == "RINNOVO") {echo "checked";}?>> RINNOVO
                            &nbsp; &nbsp;
                            <span>
                                <input id="NUOVO" type="radio" name="RINNOVO" value="NUOVO" <?php  if ($row[1] == "NUOVO") {echo "checked";}?>> NUOVO
                            </span>
                        </td>
                        <td id="mypadding">
                            DATA RICHIESTA
                            <span class="bmd-form-group">
                                <input type="date" id="RICHIESTA" class="form-control" name="RICHIESTA" min="2020-01-01"
                                    max="2020-12-31" value="<?php echo $row[2];?>" required="required">
                            </span>
                        </td>
                        <td>TESSERA N.
                            <span class="bmd-form-group">
                                <input type="text" id="TESSERA" class="form-control" name="TESSERA" value="<?php echo $row[3];?>"
                                    required="required">
                            </span>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <br>
        <div>
            <div>
                <span class="bmd-form-group">
                    <div class="input-group-custom">
                        <div class="input-group-prepend">
                            <span class="input-group-text">
                                NOME
                            </span>
                        </div>&nbsp;&nbsp;&nbsp;
                        <input type="text" id="NOME" class="form-control" name="NOME" value="<?php echo $row[4];?>" required="required">
                        &nbsp;&nbsp;&nbsp;
                        <span class="input-group-text">CONGNAME</span>&nbsp;&nbsp;&nbsp;
                        <input type="text" id="CONGNAME" class="form-control" name="CONGNAME" value="<?php echo $row[5];?>" required="required">
                    </div>
                </span>
            </div>
            <div>
                <span class="bmd-form-group">
                    <div class="input-group-custom">
                        <div class="input-group-prepend">
                            <span class="input-group-text"> Nato a</span>
                        </div>&nbsp;&nbsp;&nbsp;
                        <input type="text" id="Nato" class="form-control" name="Nato" value="<?php echo $row[6];?>" required="required">
                        &nbsp;&nbsp;&nbsp;
                        <span class="input-group-text">il</span>&nbsp;&nbsp;&nbsp;
                        <input type="date" id="il" class="form-control" name="il" value="<?php echo $row[7];?>" required="required">
                    </div>
                </span>
            </div>
            <div>
                <span class="bmd-form-group">
                    <div class="input-group-custom">
                        <div class="input-group-prepend">
                            <span class="input-group-text">Residente a
                            </span>
                        </div>&nbsp;&nbsp;&nbsp;
                        <input type="text" id="Residente" class="form-control" name="Residente" value="<?php echo $row[8];?>" required="required">
                        &nbsp;&nbsp;&nbsp;
                        <span class="input-group-text">Cap.</span>&nbsp;&nbsp;&nbsp;
                        <input type="text" id="Cap" class="form-control" name="Cap" value="<?php echo $row[10];?>" required="required">
                    </div>
                </span>
            </div>
            <div>
                <span class="bmd-form-group">
                    <div class="input-group-custom" id="valid1">
                        <div class="input-group-prepend">
                            <span class="input-group-text">Via</span>
                        </div>&nbsp;&nbsp;&nbsp;
                        <input type="number" id="Via" class="form-control" name="Via" value="<?php echo $row[9];?>" required="required">

                        &nbsp;&nbsp;&nbsp;
                        <span class="input-group-text">
                            Cell.
                        </span>&nbsp;&nbsp;&nbsp;
                        <input type="number" id="Cell" class="form-control" name="Cell" value="<?php echo $row[11];?>" required="required">

                    </div>
                </span>
            </div>
            <div>
                <span class="bmd-form-group">
                    <div class="input-group-custom">
                        <div class="input-group-prepend">
                            <span class="input-group-text">Indirizzo e-mail</span>
                        </div>&nbsp;&nbsp;&nbsp;
                        <input type="email" id="e-mail" class="form-control" name="e-mail" value="<?php echo $row[12];?>" required="required">
                    </div>
                </span>
            </div>
        </div>
        <br> <br>
        <div>
            <p>Chiede con la firma della presente di poter essere ammesso in qualità di socio all’associazione “DONK
                CLUB“</p>
            <p>Dichiaro di aver letto e di rispettare lo statuto e il regolamento del sodalizio e di accettarli in ogni
                suo punto.</p>
            <p>Dichiaro di impegnarmi al pagamento della quota associativa e ai contributi associativi a seconda
                dell’attività svolta ed alle modalità prescelte.</p>
            <p>In ogni caso prendo atto che tutte le affissioni con gli eventi, le attività e le assemblee, saranno
                riportate nell’apposito spazio destinato alle comunicazioni.</p>
            <p>Ai sensi e per gli effetti degli articoli 1341 e 1342 del C.C., dichiaro di aver letto e di aver ben
                compreso nonché di approvare espressamente le condizioni e le pattuizioni previste dallo statuto, dal
                regolamento e dal contratto assicurativo derivante dal tesseramento.</p>
            <p>Dichiaro di aver letto l’informativa sulla privacy apposta nell’apposito spazio e di firmare in calce per
                accettazione e consenso al trattamento dei dati personali ai sensi del GDPR Regolamento (UE) 2016/679
            </p>
        </div>
        <br><br>
        <div>
            <table>
                <tr id="left">
                    <td>
                        <span> Firma del Richiedente</span>
                        <i class="bmd-form-group">
                            <img src="<?php echo $row[13];?>" height='100px' width='500px' />
                        </i>
                    </td>
                    <td>
                        <span> Firma del Presidente</span>
                        <span class="bmd-form-group">
                            <img src="<?php echo $row[14];?>" height='100px' width='500px' />
                        </span>
                    </td>
                </tr>
            </table>
        </div>
        <br>
        <div class="center">
            <button class="btn btn-success" id="Print">Print</button>
        </div>
    </div>
    <div class="empty"></div>
</body>
    
<script>
    $(document).ready(function () {
        $("#Print").on('click', function(){
        window.print();
    });
    });
</script>
</html>

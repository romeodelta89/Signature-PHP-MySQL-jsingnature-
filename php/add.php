<?php

$host         = "localhost";
$username     = "root";
$password     = "";
$dbname       = "singnature";
$result = 0;

/* Create connection */
$conn = new mysqli($host, $username, $password, $dbname);
/* Check connection */
if ($conn->connect_error) {
     die("Connection to database failed: " . $conn->connect_error);
}
 
    /* Get data from Client side using $_POST array */
    $user_info = $_POST['user_info'];
    $RICHIESTA = $_POST['RICHIESTA'];
    $TESSERA = $_POST['TESSERA'];
    $NOME = $_POST['NOME'];
    $CONGNAME = $_POST['CONGNAME'];
    $Nato = $_POST['Nato'];
    $il = $_POST['il'];
    $Residente  = $_POST['Residente'];
    $Cap = $_POST['Cap'];
    $Via = $_POST['Via'];
    $Cell = $_POST['Cell'];
    $email = $_POST['email'];
    $src1 = $_POST['src1'];
    $src2 = $_POST['src2'];

    if(!$user_info || !$RICHIESTA || !$TESSERA || !$NOME || !$CONGNAME || !$Nato || !$il || !$Residente || !$Cap || !$Via || !$Cell && !$email){
        $result = 1;
    } else { 
     $sql    = "insert into user (user_info, register_date, card_number, fname, surname, birth_place, birthday, live_city, Cap, via, cell, email, user_sign, president_sign) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
     $stmt   = $conn->prepare($sql);
     $stmt->bind_param('ssssssssssssss', $user_info, $RICHIESTA, $TESSERA, $NOME, $CONGNAME, $Nato, $il, $Residente, $Cap, $Via, $Cell, $email, $src1,  $src2);
     if($stmt->execute()){
        
        $last_id = $conn->insert_id;
        $result =  $last_id;
     }
  }
  echo $result;
  $conn->close();

?>
